import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Joueur } from '../models/Joueurs';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(
    private http: HttpClient
  ) { }

  public username;
  public password1;
  public password2;
  public message;
  public compte : number;

  public listeDeJoueurs : Joueur[]

  public valider(event:any):void
  {

    if (this.username != "" && this.password1 != "" && this.password2 != "")
    {

      if (this.username.length >= 6)
      {
        if (this.password1 == this.password2)
        {
          console.log("Écriture vers l'API")
          this.http.post("http://localhost:55727/api/Joueurs", { pseudo: this.username, motDePasse: this.password1})
          .subscribe(data => {
            console.log(data)
          }, error => {
            console.log(error)
          })
        }

        else
        {
          this.message = 'Vos mots de passe sont différents !';
        }
      }

      else
      {
        this.message = 'Votre nom doit faire au moins 6 charactères !';
      }
    }

    else
    {
      this.message = 'Veuillez renseigner tout les champs !';
    }

    
  }

  ngOnInit() 
  {
    this.listeDeJoueurs = [];
    this.http.get<Joueur[]>("http://localhost:55727/api/Joueurs")
    .subscribe(x => 
      {
        console.log(x);
        this.listeDeJoueurs = x;
        for(let item of x) {
          console.log(item.pseudo)
        }
      });
      console.log(this.listeDeJoueurs);
  }

}
