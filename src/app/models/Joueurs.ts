export interface Joueur {
  id: number;
  pseudo: string;
  motDePasse: string;
  pointsDeSurvie: number;
}