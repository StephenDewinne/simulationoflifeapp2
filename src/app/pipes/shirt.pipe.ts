import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shirt'
})
export class ShirtPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return './../../assets/images/Character/Shirt/' + value + '.png';
  }

}
