import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'body'
})
export class BodyPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return './../../assets/images/Character/Body/' + value + '.png';
  }

}
