import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public body;
  public hair;
  public beard;
  public shirt;
  public body2;
  public hair2;
  public beard2;
  public shirt2;
  constructor()
  {
    
  }

  ngOnInit()
  {
    var ListeCorps : string[] = ["Body_C0_S0", "Body_C1_S0", "Body_C2_S0", "Body_C3_S0", "Body_C4_S0", "Body_C5_S0", "Body_C6_S0", "Body_C7_S0"];
    var ListeCheveux : string[] = ["None","Hair_C0_S0","Hair_C1_S0","Hair_C2_S0","Hair_C3_S0","Hair_C4_S0","Hair_C5_S0","Hair_C9_S0"]
    var ListeBarbe : string[] = ["None","Beard_C0_S0","Beard_C1_S0","Beard_C2_S0","Beard_C3_S0","Beard_C4_S0","Beard_C5_S0","Beard_C9_S0"]
    var ListeShirt : string[] = ["Jacket_C0_S0","Jacket_C1_S0","Jacket_C2_S0","Jacket_C3_S0","Jacket_C4_S0","Jacket_C5_S0","Jacket_C6_S0","Jacket_C7_S0","Jacket_C8_S0","Jacket_C9_S0",]

    this.body = ListeCorps[Math.floor(Math.random() * ListeCorps.length)]
    this.hair = ListeCheveux[Math.floor(Math.random() * ListeCheveux.length)]
    this.beard = ListeBarbe[Math.floor(Math.random() * ListeBarbe.length)]
    this.shirt = ListeShirt[Math.floor(Math.random() * ListeShirt.length)]

    this.body2 = ListeCorps[Math.floor(Math.random() * ListeCorps.length)]
    this.hair2 = ListeCheveux[Math.floor(Math.random() * ListeCheveux.length)]
    this.beard2 = ListeBarbe[Math.floor(Math.random() * ListeBarbe.length)]
    this.shirt2 = ListeShirt[Math.floor(Math.random() * ListeShirt.length)]
  }

}
